<?php
namespace Tax\Service;

use Tax\Entity\Tax;
use Tax\Entity\TaxTable;
use Zend\Filter\StaticFilter;

// The TaxManager service is responsible for adding new posts.
class TaxManager 
{
    /**
    * Doctrine entity manager.
    * @var Doctrine\ORM\EntityManager
    */
    private $entityManager;

    // Constructor is used to inject dependencies into the service.
    public function __construct($entityManager)
    {
        $this->entityManager = $entityManager;
    }

    // This method adds a new tax.
    public function addNewTax($data) 
    {
        $taxTable = $this->entityManager->getRepository(TaxTable::class)->findOneBy(array("id" => $data['tax_table_id']));

        // Create new Tax entity.
        $tax = new Tax();
        $tax->setTaxTable($taxTable);
        $tax->setValue($data['value']);
        $tax->setFromValue($data['from_value']);
        $tax->setUntilValue($data['until_value']);

        // Add the entity to entity manager.
        $this->entityManager->persist($tax);
                        
        // Apply changes to database.
        $this->entityManager->flush();
    }

    // This method allows to update data of a single tax.
    public function updateTax($tax, $data) 
    {
        $tax->setTaxTableId($data['tax_table_id']);
        $tax->setValue($data['value']);
        $tax->setFromValue($data['from_value']);
        $tax->setUntilValue($data['until_value']);
                
        // Apply changes to database.
        $this->entityManager->flush();
    }  

    // Removes tax
    public function removeTax($tax) 
    { 
        $this->entityManager->remove($tax);
        $this->entityManager->flush();
    } 
}