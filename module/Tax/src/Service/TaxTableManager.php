<?php
namespace Tax\Service;

use Tax\Entity\TaxTable;
use Tax\Entity\Operator;
use Zend\Filter\StaticFilter;

// The TaxTableManager service is responsible for adding new posts.
class TaxTableManager 
{
    /**
    * Doctrine entity manager.
    * @var Doctrine\ORM\EntityManager
    */
    private $entityManager;
  
    // Constructor is used to inject dependencies into the service.
    public function __construct($entityManager)
    {
        $this->entityManager = $entityManager;
    }
    
    // This method adds a new taxTable.
    public function addNewTaxTable($data) 
    {
        $operator = $this->entityManager->getRepository(Operator::class)->findOneBy(array("id" => $data['operator_id']));

        // Create new taxTable entity.
        $taxTable = new TaxTable();
        $taxTable->setOperator($operator);
        $taxTable->setDescription($data['description']);
        $taxTable->setEffectiveDate($data['effective_date']);

        // Add the entity to entity manager.
        $this->entityManager->persist($taxTable);
            
        // Apply changes to database.
        $this->entityManager->flush();
    }
  
    // This method allows to update data of a single taxTable.
    public function updateTaxTable($taxTable, $data) 
    {
        $taxTable->setOperatorId($data['operator_id']);
        $taxTable->setDescription($data['description']);
        $taxTable->setEffectiveDate($data['effective_date']);
        
        // Apply changes to database.
        $this->entityManager->flush();
    }  

    // Removes taxTable
    public function removeTaxTable($taxTable) 
    { 
        $this->entityManager->remove($taxTable);
        $this->entityManager->flush();
    } 
}