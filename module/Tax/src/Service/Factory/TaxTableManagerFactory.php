<?php
namespace Tax\Service\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Tax\Service\TaxTableManager;

/**
 * This is the factory for TaxManager. Its purpose is to instantiate the
 * service.
 */
class TaxTableManagerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, 
                    $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        
        // Instantiate the service and inject dependencies
        return new TaxTableManager($entityManager);
    }
}