<?php
namespace Tax\Controller\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Tax\Service\TaxTableManager;
use Tax\Controller\TaxTableController;

/**
 * This is the factory for IndexController. Its purpose is to instantiate the controller.
 */
class TaxTableControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, 
                     $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $taxTableManager = $container->get(TaxTableManager::class);
        
        // Instantiate the controller and inject dependencies
        return new TaxTableController($entityManager, $taxTableManager);
    }
}