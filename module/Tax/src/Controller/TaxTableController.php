<?php

namespace Tax\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Tax\Entity\TaxTable;
use Tax\Form\TaxTableForm;

class TaxTableController extends AbstractActionController
{
	/**
	 * Entity manager.
	 * @var Doctrine\ORM\EntityManager
	 */
	private $entityManager;

	/**
     * Tax manager.
     * @var Tax\Service\TaxTableManager 
     */
    private $taxTableManager;

	 // Constructor method is used to inject dependencies to the controller.
	public function __construct($entityManager, $taxTableManager) 
	{
		$this->entityManager = $entityManager;
        $this->taxTableManager = $taxTableManager;
	}

	public function indexAction()
    {
        // Get recent posts
	    $taxsTable = $this->entityManager->getRepository(TaxTable::class)->findAll();

	    // Render the view template
	    return new ViewModel([
	     	'taxsTable' => $taxsTable
	    ]);
    }

    /**
     * This action displays the "New TaxTable" page. The page contains 
     * a form allowing to enter post title, content and tags. When 
     * the user clicks the Submit button, a new TaxTable entity will 
     * be created.
     */
    public function addAction() 
    {
        // Create the form.
        $form = new TaxTableForm($this->entityManager);
        
        // Check whether this taxTable is a POST request.
        if ($this->getRequest()->isPost()) {
            
            // Get POST data.
            $data = $this->params()->fromPost();
            
            // Fill form with data.
            $form->setData($data);
            if ($form->isValid()) {
                                
                // Get validated form data.
                $data = $form->getData();
                // Use post manager service to add new tax to database.                
                $this->taxTableManager->addNewTaxTable($data);
                
                // Redirect the user to "index" page.
                return $this->redirect()->toRoute('tax-table');
            }
        }
        
        // Render the view template.
        return new ViewModel([
            'form' => $form
        ]);
    }   

    // This action displays the page allowing to edit a taxTable.
  public function editAction() 
  {
    // Create the form.
    $form = new TaxTableForm($this->entityManager);
    
    // Get taxTable ID.    
    $taxTableId = $this->params()->fromRoute('id', -1);
    
    // Find existing taxTable in the database.    
    $taxTable = $this->entityManager->getRepository(TaxTable::class)
                ->findOneById($taxTableId);        
    if ($taxTable == null) {
      $this->getResponse()->setStatusCode(404);
      return;                        
    } 
        
    // Check whether this taxTable is a POST request.
    if ($this->getRequest()->isPost()) {
            
      // Get POST data.
      $data = $this->params()->fromPost();
            
      // Fill form with data.
      $form->setData($data);
      if ($form->isValid()) {
                                
        // Get validated form data.
        $data = $form->getData();
                
        // Use taxTable manager service to add new taxTable to database.                
        $this->taxTableManager->updateTaxTable($taxTable, $data);
                
        // Redirect the user to "admin" page.
        return $this->redirect()->toRoute('tax-table', ['action' => 'index']);
      }
    } else {
      $data = [
               'operator_id' => $taxTable->getOperatorId(),
               'description' => $taxTable->getDescription(),
               'effective_date' => $taxTable->getEffectiveDate()
            ];
            
      $form->setData($data);
    }
        
    // Render the view template.
    return new ViewModel([
            'form' => $form,
            'taxTable' => $taxTable
        ]);  
    }

    // This "delete" action displays the Delete TaxTable page.
    public function deleteAction()
    {
        $taxTableId = $this->params()->fromRoute('id', -1);
            
        $taxTable = $this->entityManager->getRepository(TaxTable::class)
                ->findOneById($taxTableId);        
        if ($taxTable == null) {
          $this->getResponse()->setStatusCode(404);
          return;                        
        }        
            
        $this->taxTableManager->removeTaxTable($taxTable);
            
        // Redirect the user to "index" page.
        return $this->redirect()->toRoute('tax-table', ['action' => 'index']);
    }
}