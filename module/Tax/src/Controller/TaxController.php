<?php

namespace Tax\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Tax\Entity\Tax;
use Tax\Form\TaxForm;

class TaxController extends AbstractActionController
{
	/**
	 * Entity manager.
	 * @var Doctrine\ORM\EntityManager
	 */
	private $entityManager;

	/**
     * Tax manager.
     * @var Tax\Service\TaxManager 
     */
    private $taxManager;

	 // Constructor method is used to inject dependencies to the controller.
	public function __construct($entityManager, $taxManager) 
	{
		$this->entityManager = $entityManager;
        $this->taxManager = $taxManager;
	}

	public function indexAction()
    {
        // Get recent posts
	    $taxs = $this->entityManager->getRepository(Tax::class)->findAll();

	    // Render the view template
	    return new ViewModel([
	     	'taxs' => $taxs
	    ]);
    }

    /**
     * This action displays the "New Tax" page. The page contains 
     * a form allowing to enter post title, content and tags. When 
     * the user clicks the Submit button, a new Tax entity will 
     * be created.
     */
    public function addAction() 
    {
        // Create the form.
        $form = new TaxForm($this->entityManager);
        
        // Check whether this tax is a POST request.
        if ($this->getRequest()->isPost()) {
            
            // Get POST data.
            $data = $this->params()->fromPost();
            
            // Fill form with data.
            $form->setData($data);
            if ($form->isValid()) {
                                
                // Get validated form data.
                $data = $form->getData();
                // Use post manager service to add new tax to database.                
                $this->taxManager->addNewTax($data);
                
                // Redirect the user to "index" page.
                return $this->redirect()->toRoute('tax');
            }
        }
        
        // Render the view template.
        return new ViewModel([
            'form' => $form
        ]);
    }   

    // This action displays the page allowing to edit a tax.
  public function editAction() 
  {
    // Create the form.
    $form = new TaxForm($this->entityManager);
    
    // Get tax ID.    
    $taxId = $this->params()->fromRoute('id', -1);
    
    // Find existing tax in the database.    
    $tax = $this->entityManager->getRepository(Tax::class)
                ->findOneById($taxId);        
    if ($tax == null) {
      $this->getResponse()->setStatusCode(404);
      return;                        
    } 
        
    // Check whether this tax is a POST request.
    if ($this->getRequest()->isPost()) {
            
      // Get POST data.
      $data = $this->params()->fromPost();
            
      // Fill form with data.
      $form->setData($data);
      if ($form->isValid()) {
                                
        // Get validated form data.
        $data = $form->getData();
                
        // Use tax manager service to add new tax to database.                
        $this->taxManager->updateTax($tax, $data);
                
        // Redirect the user to "admin" page.
        return $this->redirect()->toRoute('tax', ['action' => 'index']);
      }
    } else {
      $data = [
               'tax_table_id' => $tax->getTaxTableId(),
               'value' => $tax->getValue(),
               'from_value' => $tax->getFromValue(),
               'until_value' => $tax->getUntilValue()
            ];
            
      $form->setData($data);
    }
        
    // Render the view template.
    return new ViewModel([
            'form' => $form,
            'tax' => $tax
        ]);  
    }

    // This "delete" action displays the Delete Tax page.
    public function deleteAction()
    {
        $taxId = $this->params()->fromRoute('id', -1);
            
        $tax = $this->entityManager->getRepository(Tax::class)
                ->findOneById($taxId);        
        if ($tax == null) {
          $this->getResponse()->setStatusCode(404);
          return;                        
        }        
            
        $this->taxManager->removeTax($tax);
            
        // Redirect the user to "index" page.
        return $this->redirect()->toRoute('tax', ['action' => 'index']);
    }
}