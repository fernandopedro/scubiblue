<?php

namespace Tax\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Tax\Entity\Tax;
use Tax\Entity\TaxTable;

/**
 * This form is used to collect post data.
 */
class TaxForm extends Form
{
    /**
     * Entity manager.
     * @var Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * Constructor.     
     */
    public function __construct($entityManager)
    {
        // Define form name
        parent::__construct('tax-form');
     
        // Set POST method for this form
        $this->setAttribute('method', 'post');
        
        $this->entityManager = $entityManager;

        $this->addElements();
        $this->addInputFilter();         
    }
    
    /**
     * This method adds elements to form (input fields and submit button).
     */
    protected function addElements() 
    {
        $taxTables = $this->entityManager->getRepository(TaxTable::class)->findAll();

        foreach ($taxTables as $taxTable) {
            $tt[$taxTable->getId()] = $taxTable->getDescription();
        }

        $this->add([           
            'type'  => 'select',
            'name' => 'tax_table_id',
            'attributes' => [
                'id' => 'tax_table_id'
            ],
            'options' => array(
                'label' => 'Selecione uma taxa de tabela',
                'value_options' => $tt
             )
        ]);

        // Add "value" field
        $this->add([
            'type'  => 'text',
            'name' => 'value',
            'attributes' => [                
                'id' => 'value'
            ],
            'options' => [
                'label' => 'Valor da taxa',
            ],
        ]);
        
        // Add "from_value" field
        $this->add([
            'type'  => 'text',
            'name' => 'from_value',
            'attributes' => [                
                'id' => 'from_value'
            ],
            'options' => [
                'label' => 'Início da faixa de cobrança',
            ],
        ]);
        
        // Add "until_value" field
        $this->add([
            'type'  => 'text',
            'name' => 'until_value',
            'attributes' => [                
                'id' => 'until_value'
            ],
            'options' => [
                'label' => 'Fim da faixa de cobrança',
            ],
        ]);
        
        // Add the submit button
        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [                
                'value' => 'Salvar',
                'id' => 'submitbutton',
            ],
        ]);
    }
    
    /**
     * This method creates input filter (used for form filtering/validation).
     */
    private function addInputFilter() 
    {
        
        $inputFilter = new InputFilter();        
        $this->setInputFilter($inputFilter);
        
        $inputFilter->add([
                'name'     => 'tax_table_id',
                'required' => true,
                // 'filters'  => [
                //     ['name' => 'StringTrim'],
                //     ['name' => 'StripTags'],
                //     ['name' => 'StripNewlines'],
                // ],                
                // 'validators' => [
                //     [
                //         'name'    => 'StringLength',
                //         'options' => [
                //             'min' => 1,
                //             'max' => 1024
                //         ],
                //     ],
                // ],
            ]);
        
        $inputFilter->add([
                'name'     => 'value',
                'required' => true,
                // 'filters'  => [                    
                //     ['name' => 'StripTags'],
                // ],                
                // 'validators' => [
                //     [
                //         'name'    => 'StringLength',
                //         'options' => [
                //             'min' => 1,
                //             'max' => 4096
                //         ],
                //     ],
                // ],
            ]);   
        
        $inputFilter->add([
                'name'     => 'from_value',
                'required' => true,
                // 'filters'  => [                    
                //     ['name' => 'StringTrim'],
                //     ['name' => 'StripTags'],
                //     ['name' => 'StripNewlines'],
                // ],                
                // 'validators' => [
                //     [
                //         'name'    => 'StringLength',
                //         'options' => [
                //             'min' => 1,
                //             'max' => 1024
                //         ],
                //     ],
                // ],
            ]);

        $inputFilter->add([
                'name'     => 'until_value',
                'required' => true,
                // 'filters'  => [                    
                //     ['name' => 'StringTrim'],
                //     ['name' => 'StripTags'],
                //     ['name' => 'StripNewlines'],
                // ],                
                // 'validators' => [
                //     [
                //         'name'    => 'StringLength',
                //         'options' => [
                //             'min' => 1,
                //             'max' => 1024
                //         ],
                //     ],
                // ],
            ]);
    }
}