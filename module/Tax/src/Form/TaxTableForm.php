<?php

namespace Tax\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Tax\Entity\TaxTable;
use Tax\Entity\Operator;

/**
 * This form is used to collect taxTable data.
 */
class TaxTableForm extends Form
{
    /**
     * Entity manager.
     * @var Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * Constructor.     
     */
    public function __construct($entityManager)
    {
        // Define form name
        parent::__construct('tax-table-form');
     
        // Set POST method for this form
        $this->setAttribute('method', 'post');
        
        $this->entityManager = $entityManager;
                
        $this->addElements();
        $this->addInputFilter();         
    }
    
    /**
     * This method adds elements to form (input fields and submit button).
     */
    protected function addElements() 
    {
        $operators = $this->entityManager->getRepository(Operator::class)->findAll();

        foreach ($operators as $operator) {
            $op[$operator->getId()] = $operator->getName();
        }
                
        // Add "operator_id" field
        $this->add([           
            'type'  => 'select',
            'name' => 'operator_id',
            'attributes' => [
                'id' => 'operator_id'
            ],
            'options' => array(
                'label' => 'Selecione um operador',
                'value_options' => $op
             )
        ]);
        
        // Add "description" field
        $this->add([
            'type'  => 'text',
            'name' => 'description',
            'attributes' => [                
                'id' => 'description'
            ],
            'options' => [
                'label' => 'Descrição da taxa',
            ],
        ]);
        
        // Add "effectiveDate" field
        $this->add([
            'type'  => 'text',
            'name' => 'effective_date',
            'attributes' => [                
                'id' => 'effective_date'
            ],
            'options' => [
                'label' => 'Data efetiva',
            ],
        ]);

        // Add the submit button
        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [                
                'value' => 'Salvar',
                'id' => 'submitbutton',
            ],
        ]);
    }
    
    /**
     * This method creates input filter (used for form filtering/validation).
     */
    private function addInputFilter() 
    {
        $inputFilter = new InputFilter();        
        $this->setInputFilter($inputFilter);
        
        $inputFilter->add([
            'name'     => 'operator_id',
            'required' => true,
        ]);
        
        $inputFilter->add([
            'name'     => 'description',
            'required' => true,
            ]);   
        
        $inputFilter->add([
            'name'     => 'effective_date',
            'required' => true,
        ]);
    }
}