<?php 

namespace Tax\Entity;

use Doctrine\ORM\Mapping AS ORM;
use Doctrine\DBAL\Types\DateTime;

// use Doctrine\Common\Collections\ArrayCollection;

/**
 * Tax_table
 * 
 * @ORM\Table(name="tax_table")
 * @ORM\Entity
 */

class TaxTable
{
	/**
	 * @var integer
	 * 
	 * @ORM\Column(name="id", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;

	/**
	 * @var integer
	 * @ORM\Column(name="operator_id", type="integer", nullable=false)
	 */
	private $operatorId;

	/**
	 * @var text
	 * @ORM\Column(name="description", type="text", nullable=false)
	 */
	private $description;

	/**
	 * @var datetime
	 * @ORM\Column(name="effective_date", type="text", nullable=false)
	 */
	private $effectiveDate;

    /**
     * One TaxTable has One Operator
     * @ORM\OneToOne(targetEntity="Tax\Entity\Operator")
     * @ORM\JoinColumn(name="operator_id", referencedColumnName="id", nullable=false)
     */
    private $operator;

    /**
     * @return object
     */
    public function getOperator()
    {
        return $this->operator;
    }
    public function setOperator(Operator $operator = null) {
        $this->operator = $operator;
        return $this;
    }

    /**
     * One TaxTable has One Tax
     * @ORM\OneToOne(targetEntity="Tax", mappedBy="taxTable")
     */
    private $tax;

    public function setTax(Tax $tax = null) {
        $this->tax = $tax;
        $tax->setTaxTable($this);
        return $this;
    }

	/**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getOperatorId()
    {
        return $this->operatorId;
    }
    /**
     * @param int $operatorId
     */
    public function setOperatorId($operatorId)
    {
        $this->operatorId = $operatorId;
    }    

    /**
     * @return text
     */
    public function getDescription()
    {
        return $this->description;
    }
    /**
     * @param text $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return text
     */
    public function getEffectiveDate()
    {
        return $this->effectiveDate;
    }
    /**
     * @param text $effectiveDate
     */
    public function setEffectiveDate($effectiveDate)
    {
        $this->effectiveDate = $effectiveDate;
    }
}
