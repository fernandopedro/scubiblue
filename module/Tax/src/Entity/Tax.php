<?php 

namespace Tax\Entity;

use Doctrine\ORM\Mapping AS ORM;
// use Doctrine\Common\Collections\ArrayCollection;

/**
 * Tax
 * 
 * @ORM\Table(name="tax")
 * @ORM\Entity
 */

class Tax
{
	/**
	 * @var integer
	 * 
	 * @ORM\Column(name="id", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;

	/**
	 * @var integer
	 * @ORM\Column(name="tax_table_id", type="integer", nullable=false)
	 */
	private $taxTableId;

	/**
	 * @var float
	 * @ORM\Column(name="value", type="float", nullable=false)
	 */
	private $value;

	/**
     * @var float
     * @ORM\Column(name="from_value", type="float", nullable=false)
     */
    private $from_value;

    /**
     * @var float
     * @ORM\Column(name="util_value", type="float", nullable=false)
     */
    private $util_value;

    /**
     * One Tax has One TaxTable
     * @ORM\OneToOne(targetEntity="TaxTable")
     * @ORM\JoinColumn(name="tax_table_id", referencedColumnName="id", nullable=false)
     */
    private $taxTable;

    /**
     * @return object
     */
    public function getTaxTable()
    {
        return $this->taxTable;
    }
    public function setTaxTable(TaxTable $taxTable = null) {
        $this->taxTable = $taxTable;
        return $this;
    }

	/**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getTaxTableId()
    {
        return $this->taxTableId;
    }
    /**
     * @param int $taxTableId
     */
    public function setTaxTableId($taxTableId)
    {
        $this->taxTableId = $taxTableId;
    }

    /**
     * @return float
     */
    public function getValue()
    {
        return $this->value;
    }
    /**
     * @param float $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return float
     */
    public function getFromValue()
    {
        return $this->from_value;
    }
    /**
     * @param float $from_value
     */
    public function setFromValue($from_value)
    {
        $this->from_value = $from_value;
    }

    /**
     * @return float
     */
    public function getUntilValue()
    {
        return $this->util_value;
    }
    /**
     * @param float $util_value
     */
    public function setUntilValue($util_value)
    {
        $this->util_value = $util_value;
    }
}
