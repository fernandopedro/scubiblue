<?php

namespace Tax\Entity;

use Doctrine\ORM\Mapping as ORM;
use Tax\Entity\TaxTable;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Operator
 * 
 * @ORM\Table(name="operator")
 * @ORM\Entity
 */

class Operator 
{
	/**
	 * @var integer
	 * 
	 * @ORM\Column(name="id", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;

	/**
	 * @var string
	 * 
	 * @ORM\Column(name="name",  type="string", nullable=false, length=50)
	 */
	private $name;

    /**
     * One Operator has One TaxTable
     * @ORM\OneToOne(targetEntity="TaxTable", mappedBy="operator")
     */
    private $taxTable;

    public function setTaxTable(TaxTable $taxTable = null) {
        $this->taxTable = $taxTable;
        $taxTable->setOperator($this);
        return $this;
    }

	/**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }
}
