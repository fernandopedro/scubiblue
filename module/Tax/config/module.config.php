<?php

namespace Tax;

use Zend\ServiceManager\Factory\InvokableFactory;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Zend\Router\Http\Segment;
use Zend\Router\Http\Literal;
return [
	'controllers' => [
        'factories' => [
			Controller\IndexController::class => InvokableFactory::class,
            Controller\TaxController::class => Controller\Factory\TaxControllerFactory::class,    
			Controller\TaxTableController::class => Controller\Factory\TaxTableControllerFactory::class,
		]
	],
    'service_manager' => [
        'factories' => [
            Service\TaxManager::class => Service\Factory\TaxManagerFactory::class,  
            Service\TaxTableManager::class => Service\Factory\TaxTableManagerFactory::class,  
        ]
    ],
	'router' => [
		'routes' => [
			'home' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'tax' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/tax[/:action[/:id]]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]*'
                    ],
                    'defaults' => [
                        'controller'    => Controller\TaxController::class,
                        'action'        => 'index',
                    ],
                ],
            ],
            'tax-table' => [
                'type' => 'literal',
                'options' => [
                    'route'    => '/taxTable',
                    'defaults' => [
                        'controller' => Controller\TaxTableController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'tax-table' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/taxTable[/:action[/:id]]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]*'
                    ],
                    'defaults' => [
                        'controller'    => Controller\TaxTableController::class,
                        'action'        => 'index',
                    ],
                ],
            ],
		]
	],

	'view_manager' => [
		'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => [
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'tax/index/index' => __DIR__ . '/../view/tax/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ],
		'template_path_stack' => [
            __DIR__ . '/../view',
        ],
	],
	'doctrine' => [
        'driver' => [
            __NAMESPACE__ . '_driver' => [
                'class' => AnnotationDriver::class,
                'cache' => 'array',
                'paths' => [__DIR__ . '/../src/Entity']
            ],
            'orm_default' => [
                'drivers' => [
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                ]
            ]
        ]
    ]  
];